package location.analysis

# defines resource types and values
resource_type := "azurerm_resource_group"
location := "westeurope"

# check resource location
deny[msg] {
    changeset := input.resource_changes[_]
    
    is_create_or_update(changeset.change.actions)

    changeset.type == resource_type
    changeset.change.after.location != location
    
    msg := sprintf("RG %v does not match allowed locations", [
        changeset.name
    ])
}

# check actions
is_create_or_update(actions) { actions[_] == "create" }
is_create_or_update(actions) { actions[_] == "update" }
